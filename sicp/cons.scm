(define (cons x y)
	(lambda (m) (m x y))
)

(define (car m)
	(m (lambda (p q) p))
)

(define (cdr m)
	(m (lambda (p q) q))
)

(define one-two (cons 1 2))
(display (car one-two))
(newline)
(display (cdr one-two))
(newline)
